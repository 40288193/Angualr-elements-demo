# Angualr elements demo

#### 项目介绍
这是如何在非Angular项目中， 引用Angular Elements的打包结构的实例！参见博客：https://my.oschina.net/u/1540190/blog/1929672

#### 使用说明
双击 demo.html 在各种浏览器中打开。从控制台中，观察 angluar elements节点在页面中的样子

切换文件中，引用external-dashboard-tile.xx.js   的引用路径，再打开浏览器控制台，观察节点的样子！
